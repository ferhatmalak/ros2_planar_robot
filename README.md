# ros2_planar_robot

## November $24^{th}$ 2023

### Intermediary Goal (1h)

- Based on [this](https://docs.ros.org/en/foxy/Tutorials/Intermediate/Launch/Creating-Launch-Files.html) tutorial, write a launch file `ros2_planar_robot_launch.py` in a folder `launch` that initiates the nodes `trajectory_generator`, `controller`, `kinematic_model`, `simulator` and `disturbance_generator`.

- The launch file should set the parameters `l1` and `l2`, as described in [this](https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Using-Parameters-In-A-Class-CPP.html) tutorial.

- The whole system should work properly whe the following commands are performed

    - terminal 1: `ros2 launch ros2_planar_robot ros2_planar_robot_launch.py`
    - terminal 2: `ros2 run ros2_planar_robot high_lev_mng`  &rarr;   enter trajectory ref_poses
    - terminal 3: `ros2 topic pub -1 /launch std_msgs/msg/Bool "{data: true}"`

### Final Assignment - commit due December 1st @23h59

- The current commit includes the following files

    - `urdf/planar_robot.urdf.xml`: urdf description of a 2 DoF robot with $l_1 = l_2 = 1.0$ m.
    - `urdf/planar_robot.rviz`: configuration rviz file 
    - `launch/simple_visualizer.py`: launches a `tf` static broadcaster and a `robot_state_publisher`.

- Necessary tutorials:

    - [Introducing `tf2`](https://docs.ros.org/en/foxy/Tutorials/Intermediate/Tf2/Introduction-To-Tf2.html)
    - Section "The proper way to publish static transform" of [`tf2` static broadcaster tutorial](https://docs.ros.org/en/foxy/Tutorials/Intermediate/Tf2/Writing-A-Tf2-Static-Broadcaster-Cpp.html#the-proper-way-to-publish-static-transforms)
    - [Using URDF with `robot_state_publisher`](https://docs.ros.org/en/foxy/Tutorials/Intermediate/URDF/Using-URDF-with-Robot-State-Publisher.html)

- In order to visualize a robot with joint positions $\{q_0,q_1\}$, as described in [this tutorial](https://docs.ros.org/en/foxy/Tutorials/Intermediate/URDF/Using-URDF-with-Robot-State-Publisher.html), four things are necessary:
    - launch rviz with the configuration `urdf/planar_robot.rviz`, using
    ```
    rviz2 -d install/ros2_planar_robot/share/ros2_planar_robot/urdf/planar_robot.rviz
    ```
    - launch a `robot_state_publisher`, in charge of publishing the coordinate frames of each link.

    - Broadcast a coordinate frame `odom`, with child frame `axis`

    - Publish $\{q_0,q_1\}$ in the topic `/joint_states` with type `JointStates`.


- **Hint** &rarr; In order to create a message of type `JointState` in C++, the following lines of code can be used:
```
auto message  = sensor_msgs::msg::JointState();
rclcpp::Time now = this->get_clock()->now();
message.header.stamp = now;
message.name = {"joint_name_1","joint_name_2"}; // Check joint names in the urdf file
message.position = {q0,q1};
```

### Main Goal:

Develop a node `joint_pub` that publishes joint positions in the `/joint_states` topic based on the `/q` topic, such that the robot motion is visualized in rviz.

------------------------------------------------------------------------------------

### Answers

#### 1. Explain what the nodes launched in `launch/simple_visualizer.py` file do.
the function `generate_launch_description()` defines and configures the nodes that should be launched.

`urdf_file_name = 'urdf/planar_robot.urdf.xml'`: Specifies the name of the URDF file for the planar robot. 
`urdf = os.path.join(get_package_share_directory('ros2_planar_robot'), urdf_file_name)`: Constructs the full path to the URDF file.
`with open(urdf, 'r') as infp: robot_desc = infp.read()`: Opens the URDF file and reads its content into the robot_desc variable. This variable will be used to pass the robot's description to the robot_state_publisher node.


the launch file launches two crucial nodes for robot visualization: 
`The static_transform_publisher` node establishes a static transformation between the "odom" (frame_id) and "axis" (child_frame_id) frames with a (0, 0, 0)translation and a (0, 0, 0) rotation . This is useful for defining a fixed relationship between two coordinate frames.

Launches the robot_state_publisher node, which reads the URDF description (robot_desc) and broadcasts the robot's state over the ROS network. The output='screen' option redirects output to the console for monitoring.

The `robot_state_publisher` node reads the URDF description of a planar robot from robot_desc file and broadcasts the robot's state over the ROS network. The output='screen' option redirects output to the console for monitoring. This information is essential for visualizing the robot's configuration in RViz.
Together, these nodes contribute to the setup and visualization of the planar robot


#### 2. Write here the instructions to demonstrate the functionality of the developed solutions:
terminal 01 :( from the work space we launch the trajectory generation, the kinematic modeller, the controller , the simulator , the joint publisher ,the launch node and the disturbance  ) `ros2 launch ros2_planar_robot ros2_planar_robot_launch.py`

terminal 02 :(from the launch file of the ros2_planar_robot package we publish the teh message true ) 
`ros2 topic pub -1 /launch std_msgs/msg/Bool "{data: true}"`

terminal 03 : (from the work space we run the the high level manager run ros2_planar_robot high_lev_mng
 and we unter the desired number of point and there coordinates) `ros2 run ros2_planar_robot high_lev_mng`


terminal 04 :(from the work space we initilize the Rviz with the configuration file (planar_robot.rviz) for visualization)  `rviz2 -d install/ros2_planar_robot/share/ros2_planar_robot/urdf/planar_robot.rviz`

terminal 05 :(from the launch file of the ros2_planar_robot package we initilize the visualization of the planar robot ) `ros2 launch simple_visualizer.py `

we can starte by initilizing the planar robot visualization , then after initilizing the ros2_planar_robot_launch.py the robot startes moving due to the disturbance, the after setting the final positions the the high level node , the robot start following the generated trajectory .



 `The Transform Library (TF2)` is a tool for managing coordinate frames and transformations, and it is used for visualizing and coordinating the planar robots. To visualize a planar robot, TF2 is employed to broadcast and manage the transformations between the two coordinate frames associated with the robot's components. Visualization tools like RViz was then subscribe to these transformations and render an accurate representation of the planar robot's configuration in real-time. By utilizing TF2, we ensure that the visual representation aligns with the actual state of the robot, providing a valuable debugging and monitoring tool for understanding the robot's movements and interactions with its environment. This integration of TF2 with visualization tools enhances the development and debugging process, contributing to a more intuitive understanding of the planar robot's behavior within the ROS 2 ecosystem.


### Multi machine execution:
step 01: physical link by ethernet cable 
step 02: ensure the connection pinging the host machine
step 03: associate both machines to the same ros domain, example  : ```export ROS_DOMAIN_ID=10``` (in each new terminal or in the bashrc for persistent domain initialization)
step 04: launch complementary nodes on each machine 

This machine launched processing nodes relative to kinematic , control and distrbance generation :
``high_lev_mng``
``trajectory_generation``
``kinematic_modeller``
``control``
``simulator``
``joint_pub``
``disturbance ``

The other machine launched the visualization of the planar robot and the comparative out put between the desired and actual kinematic in plotjuggler 


------------------------------------------------------------------------------------

### Updating your forked directory

You can update your forked repository adding this project as a remote, fetching the updated commits and merging it with your current local:

```
cd ros_workspace/src/ros2_planar_robot
git remote add prof git@gitlab.com:joao.cv/ros2_planar_robot.git
git fetch assignment_remote
git merge assignment_remote/main
```
You will probably need to solve the conflicts, selecting the desired changes between your local and this remote. You can use `vscode` for that.

------------------------------------------------------------------------------------