// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from ros2_planar_robot:msg/JointVelocity.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_VELOCITY__BUILDER_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_VELOCITY__BUILDER_HPP_

#include <algorithm>
#include <utility>

#include "ros2_planar_robot/msg/detail/joint_velocity__struct.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


namespace ros2_planar_robot
{

namespace msg
{

namespace builder
{

class Init_JointVelocity_dq
{
public:
  Init_JointVelocity_dq()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::ros2_planar_robot::msg::JointVelocity dq(::ros2_planar_robot::msg::JointVelocity::_dq_type arg)
  {
    msg_.dq = std::move(arg);
    return std::move(msg_);
  }

private:
  ::ros2_planar_robot::msg::JointVelocity msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::ros2_planar_robot::msg::JointVelocity>()
{
  return ros2_planar_robot::msg::builder::Init_JointVelocity_dq();
}

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_VELOCITY__BUILDER_HPP_
