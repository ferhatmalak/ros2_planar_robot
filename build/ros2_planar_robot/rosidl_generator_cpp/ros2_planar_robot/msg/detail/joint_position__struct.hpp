// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ros2_planar_robot:msg/JointPosition.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__STRUCT_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__STRUCT_HPP_

#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

#include "rosidl_runtime_cpp/bounded_vector.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


#ifndef _WIN32
# define DEPRECATED__ros2_planar_robot__msg__JointPosition __attribute__((deprecated))
#else
# define DEPRECATED__ros2_planar_robot__msg__JointPosition __declspec(deprecated)
#endif

namespace ros2_planar_robot
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct JointPosition_
{
  using Type = JointPosition_<ContainerAllocator>;

  explicit JointPosition_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      std::fill<typename std::array<double, 2>::iterator, double>(this->q.begin(), this->q.end(), 0.0);
    }
  }

  explicit JointPosition_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  : q(_alloc)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      std::fill<typename std::array<double, 2>::iterator, double>(this->q.begin(), this->q.end(), 0.0);
    }
  }

  // field types and members
  using _q_type =
    std::array<double, 2>;
  _q_type q;

  // setters for named parameter idiom
  Type & set__q(
    const std::array<double, 2> & _arg)
  {
    this->q = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ros2_planar_robot::msg::JointPosition_<ContainerAllocator> *;
  using ConstRawPtr =
    const ros2_planar_robot::msg::JointPosition_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ros2_planar_robot::msg::JointPosition_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ros2_planar_robot::msg::JointPosition_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::msg::JointPosition_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::msg::JointPosition_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::msg::JointPosition_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::msg::JointPosition_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ros2_planar_robot::msg::JointPosition_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ros2_planar_robot::msg::JointPosition_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ros2_planar_robot__msg__JointPosition
    std::shared_ptr<ros2_planar_robot::msg::JointPosition_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ros2_planar_robot__msg__JointPosition
    std::shared_ptr<ros2_planar_robot::msg::JointPosition_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const JointPosition_ & other) const
  {
    if (this->q != other.q) {
      return false;
    }
    return true;
  }
  bool operator!=(const JointPosition_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct JointPosition_

// alias to use template instance with default allocator
using JointPosition =
  ros2_planar_robot::msg::JointPosition_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__STRUCT_HPP_
