// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__JOINT_POSITION_HPP_
#define ROS2_PLANAR_ROBOT__MSG__JOINT_POSITION_HPP_

#include "ros2_planar_robot/msg/detail/joint_position__struct.hpp"
#include "ros2_planar_robot/msg/detail/joint_position__builder.hpp"
#include "ros2_planar_robot/msg/detail/joint_position__traits.hpp"

#endif  // ROS2_PLANAR_ROBOT__MSG__JOINT_POSITION_HPP_
