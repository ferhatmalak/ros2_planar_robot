// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from ros2_planar_robot:msg/CartesianState.idl
// generated code does not contain a copyright notice
#include "ros2_planar_robot/msg/detail/cartesian_state__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "rcutils/allocator.h"


// Include directives for member types
// Member `pose`
#include "ros2_planar_robot/msg/detail/pose__functions.h"
// Member `velocity`
#include "ros2_planar_robot/msg/detail/velocity__functions.h"

bool
ros2_planar_robot__msg__CartesianState__init(ros2_planar_robot__msg__CartesianState * msg)
{
  if (!msg) {
    return false;
  }
  // pose
  if (!ros2_planar_robot__msg__Pose__init(&msg->pose)) {
    ros2_planar_robot__msg__CartesianState__fini(msg);
    return false;
  }
  // velocity
  if (!ros2_planar_robot__msg__Velocity__init(&msg->velocity)) {
    ros2_planar_robot__msg__CartesianState__fini(msg);
    return false;
  }
  return true;
}

void
ros2_planar_robot__msg__CartesianState__fini(ros2_planar_robot__msg__CartesianState * msg)
{
  if (!msg) {
    return;
  }
  // pose
  ros2_planar_robot__msg__Pose__fini(&msg->pose);
  // velocity
  ros2_planar_robot__msg__Velocity__fini(&msg->velocity);
}

bool
ros2_planar_robot__msg__CartesianState__are_equal(const ros2_planar_robot__msg__CartesianState * lhs, const ros2_planar_robot__msg__CartesianState * rhs)
{
  if (!lhs || !rhs) {
    return false;
  }
  // pose
  if (!ros2_planar_robot__msg__Pose__are_equal(
      &(lhs->pose), &(rhs->pose)))
  {
    return false;
  }
  // velocity
  if (!ros2_planar_robot__msg__Velocity__are_equal(
      &(lhs->velocity), &(rhs->velocity)))
  {
    return false;
  }
  return true;
}

bool
ros2_planar_robot__msg__CartesianState__copy(
  const ros2_planar_robot__msg__CartesianState * input,
  ros2_planar_robot__msg__CartesianState * output)
{
  if (!input || !output) {
    return false;
  }
  // pose
  if (!ros2_planar_robot__msg__Pose__copy(
      &(input->pose), &(output->pose)))
  {
    return false;
  }
  // velocity
  if (!ros2_planar_robot__msg__Velocity__copy(
      &(input->velocity), &(output->velocity)))
  {
    return false;
  }
  return true;
}

ros2_planar_robot__msg__CartesianState *
ros2_planar_robot__msg__CartesianState__create()
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  ros2_planar_robot__msg__CartesianState * msg = (ros2_planar_robot__msg__CartesianState *)allocator.allocate(sizeof(ros2_planar_robot__msg__CartesianState), allocator.state);
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(ros2_planar_robot__msg__CartesianState));
  bool success = ros2_planar_robot__msg__CartesianState__init(msg);
  if (!success) {
    allocator.deallocate(msg, allocator.state);
    return NULL;
  }
  return msg;
}

void
ros2_planar_robot__msg__CartesianState__destroy(ros2_planar_robot__msg__CartesianState * msg)
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  if (msg) {
    ros2_planar_robot__msg__CartesianState__fini(msg);
  }
  allocator.deallocate(msg, allocator.state);
}


bool
ros2_planar_robot__msg__CartesianState__Sequence__init(ros2_planar_robot__msg__CartesianState__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  ros2_planar_robot__msg__CartesianState * data = NULL;

  if (size) {
    data = (ros2_planar_robot__msg__CartesianState *)allocator.zero_allocate(size, sizeof(ros2_planar_robot__msg__CartesianState), allocator.state);
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = ros2_planar_robot__msg__CartesianState__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        ros2_planar_robot__msg__CartesianState__fini(&data[i - 1]);
      }
      allocator.deallocate(data, allocator.state);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
ros2_planar_robot__msg__CartesianState__Sequence__fini(ros2_planar_robot__msg__CartesianState__Sequence * array)
{
  if (!array) {
    return;
  }
  rcutils_allocator_t allocator = rcutils_get_default_allocator();

  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      ros2_planar_robot__msg__CartesianState__fini(&array->data[i]);
    }
    allocator.deallocate(array->data, allocator.state);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

ros2_planar_robot__msg__CartesianState__Sequence *
ros2_planar_robot__msg__CartesianState__Sequence__create(size_t size)
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  ros2_planar_robot__msg__CartesianState__Sequence * array = (ros2_planar_robot__msg__CartesianState__Sequence *)allocator.allocate(sizeof(ros2_planar_robot__msg__CartesianState__Sequence), allocator.state);
  if (!array) {
    return NULL;
  }
  bool success = ros2_planar_robot__msg__CartesianState__Sequence__init(array, size);
  if (!success) {
    allocator.deallocate(array, allocator.state);
    return NULL;
  }
  return array;
}

void
ros2_planar_robot__msg__CartesianState__Sequence__destroy(ros2_planar_robot__msg__CartesianState__Sequence * array)
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  if (array) {
    ros2_planar_robot__msg__CartesianState__Sequence__fini(array);
  }
  allocator.deallocate(array, allocator.state);
}

bool
ros2_planar_robot__msg__CartesianState__Sequence__are_equal(const ros2_planar_robot__msg__CartesianState__Sequence * lhs, const ros2_planar_robot__msg__CartesianState__Sequence * rhs)
{
  if (!lhs || !rhs) {
    return false;
  }
  if (lhs->size != rhs->size) {
    return false;
  }
  for (size_t i = 0; i < lhs->size; ++i) {
    if (!ros2_planar_robot__msg__CartesianState__are_equal(&(lhs->data[i]), &(rhs->data[i]))) {
      return false;
    }
  }
  return true;
}

bool
ros2_planar_robot__msg__CartesianState__Sequence__copy(
  const ros2_planar_robot__msg__CartesianState__Sequence * input,
  ros2_planar_robot__msg__CartesianState__Sequence * output)
{
  if (!input || !output) {
    return false;
  }
  if (output->capacity < input->size) {
    const size_t allocation_size =
      input->size * sizeof(ros2_planar_robot__msg__CartesianState);
    rcutils_allocator_t allocator = rcutils_get_default_allocator();
    ros2_planar_robot__msg__CartesianState * data =
      (ros2_planar_robot__msg__CartesianState *)allocator.reallocate(
      output->data, allocation_size, allocator.state);
    if (!data) {
      return false;
    }
    // If reallocation succeeded, memory may or may not have been moved
    // to fulfill the allocation request, invalidating output->data.
    output->data = data;
    for (size_t i = output->capacity; i < input->size; ++i) {
      if (!ros2_planar_robot__msg__CartesianState__init(&output->data[i])) {
        // If initialization of any new item fails, roll back
        // all previously initialized items. Existing items
        // in output are to be left unmodified.
        for (; i-- > output->capacity; ) {
          ros2_planar_robot__msg__CartesianState__fini(&output->data[i]);
        }
        return false;
      }
    }
    output->capacity = input->size;
  }
  output->size = input->size;
  for (size_t i = 0; i < input->size; ++i) {
    if (!ros2_planar_robot__msg__CartesianState__copy(
        &(input->data[i]), &(output->data[i])))
    {
      return false;
    }
  }
  return true;
}
