// generated from rosidl_typesupport_introspection_cpp/resource/idl__type_support.cpp.em
// with input from ros2_planar_robot:msg/KinData.idl
// generated code does not contain a copyright notice

#include "array"
#include "cstddef"
#include "string"
#include "vector"
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_interface/macros.h"
#include "ros2_planar_robot/msg/detail/kin_data__struct.hpp"
#include "rosidl_typesupport_introspection_cpp/field_types.hpp"
#include "rosidl_typesupport_introspection_cpp/identifier.hpp"
#include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace ros2_planar_robot
{

namespace msg
{

namespace rosidl_typesupport_introspection_cpp
{

void KinData_init_function(
  void * message_memory, rosidl_runtime_cpp::MessageInitialization _init)
{
  new (message_memory) ros2_planar_robot::msg::KinData(_init);
}

void KinData_fini_function(void * message_memory)
{
  auto typed_message = static_cast<ros2_planar_robot::msg::KinData *>(message_memory);
  typed_message->~KinData();
}

size_t size_function__KinData__jacobian(const void * untyped_member)
{
  (void)untyped_member;
  return 4;
}

const void * get_const_function__KinData__jacobian(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::array<double, 4> *>(untyped_member);
  return &member[index];
}

void * get_function__KinData__jacobian(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::array<double, 4> *>(untyped_member);
  return &member[index];
}

void fetch_function__KinData__jacobian(
  const void * untyped_member, size_t index, void * untyped_value)
{
  const auto & item = *reinterpret_cast<const double *>(
    get_const_function__KinData__jacobian(untyped_member, index));
  auto & value = *reinterpret_cast<double *>(untyped_value);
  value = item;
}

void assign_function__KinData__jacobian(
  void * untyped_member, size_t index, const void * untyped_value)
{
  auto & item = *reinterpret_cast<double *>(
    get_function__KinData__jacobian(untyped_member, index));
  const auto & value = *reinterpret_cast<const double *>(untyped_value);
  item = value;
}

static const ::rosidl_typesupport_introspection_cpp::MessageMember KinData_message_member_array[2] = {
  {
    "pose",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    ::rosidl_typesupport_introspection_cpp::get_message_type_support_handle<ros2_planar_robot::msg::Pose>(),  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ros2_planar_robot::msg::KinData, pose),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    nullptr,  // fetch(index, &value) function pointer
    nullptr,  // assign(index, value) function pointer
    nullptr  // resize(index) function pointer
  },
  {
    "jacobian",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_DOUBLE,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    4,  // array size
    false,  // is upper bound
    offsetof(ros2_planar_robot::msg::KinData, jacobian),  // bytes offset in struct
    nullptr,  // default value
    size_function__KinData__jacobian,  // size() function pointer
    get_const_function__KinData__jacobian,  // get_const(index) function pointer
    get_function__KinData__jacobian,  // get(index) function pointer
    fetch_function__KinData__jacobian,  // fetch(index, &value) function pointer
    assign_function__KinData__jacobian,  // assign(index, value) function pointer
    nullptr  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers KinData_message_members = {
  "ros2_planar_robot::msg",  // message namespace
  "KinData",  // message name
  2,  // number of fields
  sizeof(ros2_planar_robot::msg::KinData),
  KinData_message_member_array,  // message members
  KinData_init_function,  // function to initialize message memory (memory has to be allocated)
  KinData_fini_function  // function to terminate message instance (will not free memory)
};

static const rosidl_message_type_support_t KinData_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &KinData_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace msg

}  // namespace ros2_planar_robot


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<ros2_planar_robot::msg::KinData>()
{
  return &::ros2_planar_robot::msg::rosidl_typesupport_introspection_cpp::KinData_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, ros2_planar_robot, msg, KinData)() {
  return &::ros2_planar_robot::msg::rosidl_typesupport_introspection_cpp::KinData_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif
