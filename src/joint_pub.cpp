#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "geometry_msgs/msg/twist.hpp"
#include "eigen3/Eigen/Dense"
#include "rclcpp/rclcpp.hpp"
#include "ros2_planar_robot/msg/joint_position.hpp"
#include "sensor_msgs/msg/joint_state.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

// Node publishing joint states that are used to visualize the robot in rviz
class JointPub : public rclcpp::Node
{
  public:
    JointPub()
    : Node("JointPub")
    {
      sub_q_ =  this ->create_subscription<ros2_planar_robot::msg::JointPosition>(
        "q", 10, std::bind(&JointPub::topic_callback_,this, _1)
      );

      pub_JntPst_ = this -> create_publisher <sensor_msgs::msg::JointState>("joint_states", 10);


    }

  private:
  void topic_callback_ (ros2_planar_robot::msg::JointPosition::SharedPtr joint_position){
    auto Actual_JntPst = sensor_msgs::msg::JointState();
    rclcpp::Time now = this -> get_clock() -> now();
    Actual_JntPst.header.stamp = now;
    Actual_JntPst.name = {"r1", "r2"};
    Actual_JntPst.position = {joint_position -> q[0] , joint_position-> q[1]};

    pub_JntPst_-> publish(Actual_JntPst);



  }


  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr pub_JntPst_;
  rclcpp::Subscription<ros2_planar_robot::msg::JointPosition>::SharedPtr  sub_q_ ;

  
};
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<JointPub>());
  rclcpp::shutdown();
  return 0;
}